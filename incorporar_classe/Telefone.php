<?php

declare(strict_types=1);

namespace Alura\IncorporarClasse;

class Telefone
{    
    private $ddd;
    private $telefone;
    private $tipoTelefone;

    public function __construct($ddd, $telefone, $tipoTelefone)
    {        
        $this->ddd = $ddd;
        $this->telefone = $telefone;
        $this->tipoTelefone = $tipoTelefone;
    }

    public function getDdd(): string
    {
        return $this->ddd;
    }

    public function getTelefone(): string
    {
        return $this->telefone;
    }

    public function getTipoTelefone(): string
    {
        return $this->tipoTelefone;
    }

    public function getTelefoneDdd()
    {
        return "{$this->tipoTelefone} {$this->ddd} {$this->telefone}";
    }
}