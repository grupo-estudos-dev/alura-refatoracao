<?php declare(strict_types=1);

function recebeDesconto(int $descontoInicial, bool $ehPremium, int $quantidade, int $anosCliente): void
{
    $descontoAtual = $descontoInicial;
    if ($descontoAtual > 200) {
        return;
    }

    if ($ehPremium === true) {
        $descontoAtual = $descontoAtual * 1.1;
    }

    if ($quantidade > 50) {
        $descontoAtual = $descontoAtual * 1.2;
    }

    if ($anosCliente > 3) {
        $descontoAtual = $descontoAtual * 1.1;
    }

    echo <<<EOF
--------------------
Desconto: $descontoAtual
Desconto Inicial: $descontoInicial
--------------------
EOF;
}

recebeDesconto(50, true, 100, 100);
